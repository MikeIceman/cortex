# Changelog

All notable changes to `MikeIceman/cortex` will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v1.0.0-alpha1] - 2021-09-06
### Added
- Initial Symfony project structure
- Base Docker configuration
- Base tests configuration
- Base CI/CD configuration
