<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SimpleTest extends KernelTestCase
{
    public function testSomething(): void
    {
        self::bootKernel();

        $stack = [];
        $this->assertCount(0, $stack);

        $stack[] = 'foo';
        $this->assertSame('foo', $stack[count($stack) - 1]);
        $this->assertCount(1, $stack);

        $this->assertSame('foo', array_pop($stack));
        $this->assertCount(0, $stack);
    }
}
