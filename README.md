# Cortex PHP Developer Introductory Project

[![PHP version](https://img.shields.io/badge/php-%5E7.4-blue)](https://php.com)
[![Symfony](https://img.shields.io/badge/Powered_by-Symfony_Framework_5.4-green.svg?style=flat)](https://symfony.com/)
[![pipeline status](https://gitlab.com/MikeIceman/cortex/badges/develop/pipeline.svg)](https://gitlab.com/MikeIceman/cortex/-/commits/develop)
[![coverage report](https://gitlab.com/MikeIceman/cortex/badges/develop/coverage.svg)](https://gitlab.com/MikeIceman/cortex/-/commits/develop)

**WARNING**: This is only a demo application! **Viewer discretion is advised.** 

## Description of the basic project
The task is to create a customer database structure and basic parts of the
application for managing the customer database and customer shopping
behavior.

The CRM customer database contains information about the customer (name,
surname, address, email, phone number, date of registration) and his customer
loyalty cards (card number, card type - plastic /virtual). Cards are pre-created in
the database and then assigned to customers. Each card can have at most one
owner.

Furthermore, individual purchases are registered. Each purchase contains
information about the used customer loyalty card + all basic data about the
purchase. Each purchase includes a list of bill items, which contain information
about the purchased goods, the number of pieces, the price per item, or more.

## Requirements for the implementation of the basic project
1. Create a data model (for example, ER diagram), create an SQL
  database (for example, MySQL, PostgreSQL)
2. Implement all the necessary parts of the application to complete
  the task:
   - Implement a customer registration interface
     (registration form must contain all customer data +
     plastic loyalty card assignment)
   - Implement an interface for seeking the customer by:
     _name_, _surname_ and _card number_
   - Create a page with basic reports:
     - The number of customers
     - Number of cards assigned
     - TOP 10 customers according to the turnover
       of purchases in the last 30 days

## Optional Extension Tasks (TODO)
1. Full-text search in the list of customers
1. Design of customer segmentation solutions and linked analytical outputs
1. Design of REST API for application from control task
1. Orchestration of the new customer registration process proposal

## Prerequisites
- [Docker](https://docs.docker.com/engine/install/)

## Getting started

### 1. Clone git repository
```shell
git clone git@gitlab.com:MikeIceman/cortex.git
cd cortex
```

### 2. Build and run Docker containers
You can run docker container by using following command:
```shell
docker-compose up -d --build
```
The web application will be available at [localhost](http://localhost)

RabbitMQ admin panel will be available at [http://localhost:15672/](http://localhost:15672/)

### 3. Install composer dependencies
Go inside the container and install composer dependencies
```shell
docker-compose exec php bash
composer install
```

### 4. (optional) Configure internal storage for uploads
By default, uploaded files are stored in [storage](storage) directory.

You can configure another storage directory in [services.yaml](config/services.yaml):

```yaml
# config/services.yaml

parameters:
    storage_directory: '/tmp/my_own_storage'
```

## Running tests (included scripts)

1. Run unit tests without generating of coverage report:
```shell
composer test
```

2. Run unit tests with coverage report:
```shell
composer test-coverage
```

3. Check code style according to PSR standards
```shell
composer check-cs
```

4. Automatically fix code style according to previous recommendations
```shell
composer fix-cs
```

5. Run phpStan
```shell
composer phpstan
```

6. Run Psalm
```shell
composer psalm
```
