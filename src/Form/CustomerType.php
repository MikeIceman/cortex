<?php

namespace App\Form;

use App\Entity\Card;
use App\Entity\Customer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('address', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', TelType::class)
            ->add(
                'card',
                EntityType::class,
                [
                    'class' => Card::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->andWhere('c.owner IS NULL')
                            ->orderBy('c.number', Criteria::ASC);
                    },
                    'choice_label' => function ($card) {
                        $type = $card->getType() === \App\Enum\CardType::PLASTIC ? 'Plastic' : 'Virtual';
                        return $type . ': ' . substr(str_repeat('0', 10) . $card->getNumber(), - 10);
                    }
                ]
            )
            ->add('registered', DateTimeType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
