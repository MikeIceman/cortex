<?php

namespace App\Form;

use App\Entity\Card;
use App\Entity\Customer;
use App\Entity\Order;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateType::class)
            ->add('subtotal', NumberType::class)
            ->add('total', NumberType::class)
            ->add(
                'customer',
                EntityType::class,
                [
                    'class' => Customer::class,
                    'choice_label' => function ($customer) {
                        return $customer->getId() . ': ' . $customer->getName() . ' ' . $customer->getSurname();
                    }
                ]
            )
            ->add(
                'card',
                EntityType::class,
                [
                    'class' => Card::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->andWhere('c.owner IS NULL')
                            ->orderBy('c.number', Criteria::ASC);
                        },
                    'choice_label' => function ($card) {
                        return substr(str_repeat('0', 10) . $card->getNumber(), - 10);
                    }
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
