<?php

namespace App\Form;

use App\Entity\Card;
use App\Entity\Customer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CardEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('number')
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices'  => [
                        'Virtual' => \App\Enum\CardType::VIRTUAL,
                        'Plastic' => \App\Enum\CardType::PLASTIC,
                    ]
                ]
            )
            ->add('discount')
            ->add(
                'owner',
                EntityType::class,
                [
                    'class' => Customer::class,
                    'choice_label' => function ($customer) {
                        return $customer->getId() . ': ' . $customer->getName() . ' ' . $customer->getSurname();
                    }
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Card::class,
        ]);
    }
}
