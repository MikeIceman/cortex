<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('md5', [$this, 'md5Filter']),
            new TwigFilter('money', [$this, 'moneyFilter']),
            new TwigFilter('number', [$this, 'numberFilter']),
            new TwigFilter('zeropad', [$this, 'zeropadFilter']),
        ];
    }

    public function md5Filter($string): string
    {
        return md5($string);
    }

    public function moneyFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ','): string
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        return '$' . $price;
    }

    public function numberFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ','): string
    {
        return number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    public function zeropadFilter($number): string
    {
        return substr(str_repeat('0', 10) . $number, - 10);
    }
}
