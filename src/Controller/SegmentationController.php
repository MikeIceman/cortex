<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/segmentation")
 */
class SegmentationController extends AbstractController
{
    /**
     * @Route("/", name="segment_index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository): Response
    {
        return new Response();
//        return $this->render('segment/index.html.twig', [
//            'segments' => $segmentRepository->findAll(),
//        ]);
    }
}
