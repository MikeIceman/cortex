<?php

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Customer;
use App\Form\CustomerEditType;
use App\Form\CustomerType;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use FL\QBJSParser\Parser\Doctrine\DoctrineParser;
use FL\QBJSParser\Serializer\JsonDeserializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/customer")
 */
class CustomerController extends AbstractController
{
    /**
     * @Route("/", name="customer_index", methods={"GET", "POST"})
     */
    public function index(CustomerRepository $customerRepository, Request $request): Response
    {
        $filter = $request->request->get('filter');

        if ($filter) {
            $jsonDeserializer = new JsonDeserializer();
            $customerParser = new DoctrineParser(
                Customer::class,
                [
                    'id' => 'id',
                    'name' => 'name',
                    'surname' => 'surname',
                    'email' => 'email',
                    'phone' => 'phone',
                    'registered' => 'registered',
                    'cards.id' => 'cards.id',
                    'cards.number' => 'cards.number',
                ],
                [
                    'cards' => Card::class,
                ]
            );

            $deserializedRuleGroup = $jsonDeserializer->deserialize($filter);
            $parsedRuleGroup = $customerParser->parse($deserializedRuleGroup);

            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $query = $entityManager->createQuery($parsedRuleGroup->getQueryString());
            $query->setParameters($parsedRuleGroup->getParameters());
            $customers = $query->execute();
        } else {
            $customers = $customerRepository->findAll();
        }

        return $this->render('customer/index.html.twig', [
            'customers' => $customers,
            'filter' => $filter,
        ]);
    }

    /**
     * @Route("/new", name="customer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customer);
            $entityManager->flush();

            $customer->getCard()->setOwner($customer);
            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->redirectToRoute('customer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('customer/new.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_show", methods={"GET"})
     */
    public function show(Customer $customer): Response
    {
        return $this->render('customer/show.html.twig', [
            'customer' => $customer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="customer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Customer $customer): Response
    {
        $form = $this->createForm(CustomerEditType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('customer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('customer/edit.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_delete", methods={"POST"})
     */
    public function delete(Request $request, Customer $customer): Response
    {
        if ($this->isCsrfTokenValid('delete' . $customer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($customer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('customer_index', [], Response::HTTP_SEE_OTHER);
    }
}
