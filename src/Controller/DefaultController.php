<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Customer;
use App\Entity\Order;
use App\Repository\CardRepository;
use App\Repository\CustomerRepository;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public const DAILY_GOAL = 1000000;
    /**
      * @Route("/", name="homepage")
      */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        /** @var CustomerRepository $repoCustomers */
        $repoCustomers = $em->getRepository(Customer::class);
        $totalCustomers = $repoCustomers->createQueryBuilder('c')
            // ->andWhere('c.active = 1')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();

        /** @var CardRepository $repoCards */
        $repoCards = $em->getRepository(Card::class);
        $cardsAssigned = $repoCards->createQueryBuilder('c')
            ->andWhere('c.owner IS NOT NULL')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();

        /** @var OrderRepository $repoOrders */
        $repoOrders = $em->getRepository(Order::class);
        $orders = $repoOrders->createQueryBuilder('o')
            ->andWhere('o.date >= \'' . date('Y-m-d 00:00:00') . '\'')
            ->andWhere('o.date <= \'' . date('Y-m-d 23:59:59') . '\'')
            ->select('SUM(o.total) as profit, COUNT(o.id) as count')
            ->getQuery()
            ->getOneOrNullResult();

        # region Chart
        $stats = $repoOrders->createQueryBuilder('o')
            ->select("DATE_FORMAT(o.date, '%d.%m') AS date, SUM(o.id) AS sum")
            ->groupBy('date')
            ->orderBy('o.date', Criteria::ASC)
            ->getQuery()
            ->getArrayResult();

        $series = [];
        foreach ($stats as $day) {
            $series[$day['date']] = $day['sum'];
        }
        # endregion

        # region TOP-10
        $turnover = $repoOrders->createQueryBuilder('o')
            ->select('c.name, c.surname, c.email, c.phone, SUM(o.total) AS turnover')
            ->leftJoin(Customer::class, 'c', Join::WITH, 'c.id = o.customer')
            ->groupBy('o.customer')
            ->orderBy('turnover', Criteria::DESC)
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult();
        # endregion

        return $this->render('default/index.html.twig', [
            'customers' => $totalCustomers,
            'cards' => $cardsAssigned,
            'profit' => $orders['profit'],
            'orders' => $orders['count'],
            'goal' => $orders['profit'] / static::DAILY_GOAL * 100,
            // Hack to limit charts. We can't use date functions with DQL for that while making SQL query :-(
            'series' => array_slice($series, -14, 14),
            'turnover' => $turnover,
        ]);
    }

    public function renderHeader(): Response
    {
//        $messages = ...
//        $notifications = ...
//        $tasks = ...

        return $this->render('default/header.html.twig');
    }

    public function renderSidebar($currentRoute = 'homepage'): Response
    {
        $menu = [];

//        TODO: Dynamic menu entities
//        $items = $this->getDoctrine()->getRepository(Menu::class)->findAll();
//        foreach ($items as $item) {
//            if ($item->getParent() == null) {
//                $menu[$item->getId()] = $item;
//            }
//        }

        return $this->render('default/sidebar.html.twig', [
            'menu' => $menu,
            'curroute' => $currentRoute
        ]);
    }

    public function renderBreadcrumbs(): Response
    {
        return $this->render('default/breadcrumbs.html.twig');
    }
}
