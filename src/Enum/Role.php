<?php

declare(strict_types=1);

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @psalm-immutable
 */
class Role extends Enum
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_MANAGER = 'ROLE_MANAGER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public static function getLabel(string $value): string
    {
        $labels = [
            static::ROLE_USER => 'User',
            static::ROLE_MANAGER => 'Manager',
            static::ROLE_ADMIN => 'Administrator',
            static::ROLE_SUPER_ADMIN => 'Root',
        ];

        return $labels[$value] ?? '';
    }

}
