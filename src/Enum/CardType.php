<?php

declare(strict_types=1);

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @psalm-immutable
 */
class CardType extends Enum
{
    public const PLASTIC = 1;
    public const VIRTUAL = 2;
}
