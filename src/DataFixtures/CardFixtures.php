<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Card;
use App\Enum\CardType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class CardFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $batchSize = 100;
        $cards = $this->faker->numberBetween(500, 1000);

        // Generate card numbers
        for ($i = 1; $i < $cards; $i++) {
            $card = new Card();
            $card->setNumber($i);
            $card->setType($this->faker->numberBetween(1, 2));
            $card->setDiscount($this->faker->numberBetween(5, 15));
            $manager->persist($card);

            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear(); // Detaches all objects from Doctrine
            }
        }

        $manager->flush();
        $manager->clear();
    }
}
