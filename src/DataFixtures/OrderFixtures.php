<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\CustomerRepository;
use App\Repository\ProductRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;
    private CustomerRepository $customerRepository;
    private ProductRepository $productRepository;

    public function __construct(CustomerRepository $customerRepository, ProductRepository $productRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $customers = $this->customerRepository->findAll();
        $products = $this->productRepository->findAll();


        // Generate customer's orders
        for ($i = 0; $i < 567; $i++) {
            $customer = $customers[random_int(0, count($customers) - 1)];
            $card = $customer->getCards()[0] ?? null;
            $discount = $card ? $card->getDiscount() : 0;
            $items = [];
            $total = 0;

            // Add random products to order
            for ($ii = 0; $ii < $this->faker->numberBetween(1, 5); $ii++) {
                // Select random product and count
                $product = $products[random_int(0, count($products) - 1)];
                $count = $this->faker->numberBetween(1, 10);

                $item = new OrderItem();
                $item->setProduct($product);
                $item->setCount($count);
                $item->setPrice($product->getPrice() ?? 0.0);
                $items[] = $item;

                $total += $product->getPrice() * $count;
            }

            $order = new Order();
            $order->setCustomer($customer);
            $order->setCard($card);
            $order->setDate($this->faker->dateTimeBetween('-1 month', 'now'));
            $order->setSubtotal($total);
            $order->setTotal($total - ($discount / 100) * $total);

            $manager->persist($order);

            $manager->flush();

            /** @var OrderItem $item */
            foreach ($items as $item) {
                $item->setInvoice($order);
                $manager->persist($item);
            }

            $manager->flush();
        }
    }

    public function getDependencies(): array
    {
        return [
            CustomerFixtures::class,
        ];
    }
}
