<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use App\Enum\Role;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    private Generator $faker;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        // Create root user
        $user = new User();
        $user->setUsername('root');
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'root'
            )
        );
        $user->setEmail($this->faker->email());
        $user->setName($this->faker->firstName());
        $user->setSurname($this->faker->lastName());
        $user->addRole(Role::ROLE_SUPER_ADMIN);

        $manager->persist($user);

        // Create admin
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'admin'
            )
        );
        $user->setEmail($this->faker->email());
        $user->setName($this->faker->firstName());
        $user->setSurname($this->faker->lastName());
        $user->addRole(Role::ROLE_ADMIN);

        $manager->persist($user);

        // Create default manager
        $user = new User();
        $user->setUsername('manager');
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'manager'
            )
        );
        $user->setEmail($this->faker->email());
        $user->setName($this->faker->firstName());
        $user->setSurname($this->faker->lastName());
        $user->addRole(Role::ROLE_MANAGER);

        $manager->persist($user);

        $manager->flush();
        $manager->clear();

        // Create more managers
        $batchSize = 100;
        $managers = $this->faker->numberBetween(100, 500);
        for ($i = 0; $i < $managers; $i++) {
            $manager->persist($this->getRandomUser());

            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }

        $manager->flush();
        $manager->clear();
    }

    private function getRandomUser(): User
    {
        $user = new User();
        $user->setUsername($this->faker->userName() . $this->faker->numberBetween(1000, 9999));
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $this->faker->word()
            )
        );
        $user->setEmail($this->faker->email());
        $user->setName($this->faker->firstName());
        $user->setSurname($this->faker->lastName());
        $user->addRole(Role::ROLE_MANAGER);

        return $user;
    }
}
