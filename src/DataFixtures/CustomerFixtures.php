<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Repository\CardRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class CustomerFixtures extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;
    private CardRepository $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->faker = Factory::create();
        $this->cardRepository = $cardRepository;
    }

    public function load(ObjectManager $manager)
    {
        $customers = $this->faker->numberBetween(100, 200);

        // Load customers
        for ($i = 0; $i < $customers; $i++) {
            $customer = new Customer();
            $customer->setName($this->faker->firstName());
            $customer->setSurname($this->faker->lastName());
            $customer->setEmail($this->faker->email());
            $customer->setAddress($this->faker->address());
            $customer->setPhone($this->faker->phoneNumber());
            $customer->setRegistered($this->faker->dateTime());

            $manager->persist($customer);
            $manager->flush();

            $card = $this->cardRepository->getFirstUnused();
            $card->setOwner($customer);
            $manager->persist($card);
            $manager->flush();

            // More random
            if ($this->faker->numberBetween(1, 4) === 3) {
                // Assign one more card
                $card2 = $this->cardRepository->getFirstUnused();
                $card2->setOwner($customer);
                $manager->persist($card2);
            }

            $manager->flush();
            $manager->clear();
        }

        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            CardFixtures::class,
            ProductFixtures::class,
        ];
    }
}
