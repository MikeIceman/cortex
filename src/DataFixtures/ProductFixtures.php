<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class ProductFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $batchSize = 100;
        $products = $this->faker->numberBetween(500, 1500);

        // Generate products
        for ($i = 0; $i < $products; $i++) {
            $product = new Product();
            $product->setTitle($this->faker->word());
            $product->setDescription($this->faker->sentence(10));
            $product->setPrice($this->faker->randomNumber(4));
            $product->setImage($this->faker->word() . '.png');

            $manager->persist($product);

            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear(); // Detach
            }
        }

        $manager->flush();
        $manager->clear();
    }
}
